#!/bin/sh

# Make sure $AUTOPKGTEST_TMP is available
if [ "${AUTOPKGTEST_TMP}"F = "F" ]; then
   echo "Error: expected environment variable AUTOPKGTEST_TMP is not set."
   exit 1
fi

# Make sure $AUTOPKGTEST_ARTIFACTS is available
if [ "${AUTOPKGTEST_ARTIFACTS}"F = "F" ]; then
   echo "Error: expected environment variable AUTOPKGTEST_ARTIFACTS is not set."
   exit 1
fi

# Determine the test execution mode 
if [ "${1}"F = "F" ]; then
   MODE="autopkgtest"
elif [ "${1}" = "-r" ]; then
   MODE="debian/rules"
else 
   echo "usage: $0 [-r]\n-r Run in debian/rules mode instead of autopkgtest mode"
   exit 2
fi

# Determine locations of required tools
SOURCE_TREE="${PWD}"
if [ "${MODE}" = "debian/rules" ]; then
   # In debian/rules mode, use the library code from the source tree
   PYTHON="/usr/bin/python3"
   export PYTHONPATH="${SOURCE_TREE}/src"
else
   # In autopkgtest mode, use the installed library code in the system path
   PYTHON="/usr/bin/python3"
fi

# Print a test summary
echo ""
echo "========================================================================="
echo "Running ${0} in mode: ${MODE}"
echo "========================================================================="
echo "SOURCE_TREE..........: ${SOURCE_TREE}"
echo "AUTOPKGTEST_TMP......: ${AUTOPKGTEST_TMP}"
echo "AUTOPKGTEST_ARTIFACTS: ${AUTOPKGTEST_ARTIFACTS}"
echo "PYTHON...............: ${PYTHON}"
echo "PYTHONPATH...........: ${PYTHONPATH}"
echo "========================================================================="
echo ""

# Always run tests from within $AUTOPKGTEST_TMP
cd ${AUTOPKGTEST_TMP}

# Copy the test suite into the working directory
cp -r ${SOURCE_TREE}/tests .

# Run the test suite
${PYTHON} -m unittest discover -s tests -t tests
if [ $? != 0 ]; then
   exit 1
fi

# Close the test
echo ""

